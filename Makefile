OBJ = main
SRC = $(OBJ).tex
PDF = $(OBJ).pdf
THEME = $(wildcard *.sty *.cls)
TEXC := xelatex
TEXC_OPTS += -shell-escape

.PHONY: all clean

all: $(PDF)

$(PDF): $(THEME) $(SRC)
	$(TEXC) $(TEXC_OPTS) $(SRC)
	$(TEXC) $(TEXC_OPTS) $(SRC)

clean:
	@git clean -xfd
	rm -f $(PDF)
